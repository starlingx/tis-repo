Setting up a New Development Environment
========================================

The following instructions use VirtualBox and Vagrant to setup a
VM running Debian, with most of the required tools installed.

Step 1: Download and install VirtualBox

Step 2: Download and install vagrant

Step 3: Update defaults.yaml or create a custom.yaml with the overridden values

Step 4: "vagrant up"

Note: 
 Vagrant only allows you to bring up one environment per vagrant file.
To setup additional development environments (ie: for alternative releases),
you must copy the repo to another location.
